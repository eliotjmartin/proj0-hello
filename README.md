# Proj0-Hello
-------------

Author: Eliot Martin

email: eliotm@uoregon.edu

## Description:
---------------

Trivial project to gain experience with git version control. When hello.py is run, "Hello, World!" should be printed.

